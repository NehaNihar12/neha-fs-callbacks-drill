const fs = require('fs')
var path = require('path');
const readFromFilepath = path.resolve(__dirname, 'lipsum.txt')
const containerPath = path.resolve(__dirname, 'filenames.txt')

function readFile(filepath, callback) {
  fs.readFile(filepath, 'utf8', (err, data) => {
    if (err) {
      console.error(err)
    } else {
      console.log(`read file ${path.basename(filepath)}`)
      callback(data);
    }
  })
}
function writeFile(newfilepath, data, callback) {
  fs.writeFile(newfilepath, data, (err) => {
    if (err) {
      console.error(err)
    } else {
      console.log(`finsihed writing in ${path.basename(newfilepath)}`)
      callback(newfilepath)
    }
  })
}
function writeFileNamesInAFile(newfilepath, containerPath, cb) {
  let filename = path.parse(newfilepath).name;
  writeFile(containerPath, filename, () => {
    console.log('saved', filename, 'in', path.basename(containerPath));
    cb(newfilepath);

  })
}
function getDataInLowerCaseAndSplitInSentences(data) {
  data = data.toLowerCase()
  data = data.split(/[\.\n]+/g).filter((str) => str !== ' ').join('\n');
  return data;
}

function getLowerCaseSorted(data,) {
  data = data.split('\n').sort().join('\n');
  return data;
}
function appendFile(newfilePath, containerPath, cb) {
  let filename = path.parse(newfilePath).name;
  fs.appendFile(containerPath, '\n' + filename, 'utf8', (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log(`${filename} appended in ${path.basename(containerPath)}`)
      cb(newfilePath);
    }
  })
}
//Read the contents of filenames.txt
//delete all files in list simultaneously

function deleteAllNewFiles(folderPath, filePathOfFilenamesData) {
  let directory = folderPath;
  readFile(filePathOfFilenamesData, (data) => {
    data = data.split('\n')
      .reduce((acc, val) => ({ ...acc, [val]: val }), {});

    fs.readdir(directory, (err, files) => {
      if (err) {
        console.error(err);
      } else {
        for (const file of files) {
          let filename = path.parse(file).name;
          if (data.hasOwnProperty(filename)) {
            fs.unlink(path.join(directory, file), err => {
              if (err) throw err;
              console.log(`deleted ${path.basename(file)}`)
            });
          }
        }
      }
    });

  })
}

//Read file and store uppercase data in new file
function problem2(readFromFilepath, containerPath) {
  readFile(readFromFilepath, function (data) {
    data = data.toUpperCase();
    let newfilepath = path.resolve(__dirname, 'dataInUpperCase.txt');
    writeFile(newfilepath, data, (newfilepath) => {
      writeFileNamesInAFile(newfilepath, containerPath, (newfilepath) => {
        readFile(newfilepath, (data) => {
          data = getDataInLowerCaseAndSplitInSentences(data)
          newfilepath = path.resolve(__dirname, 'dataInLowerCase.txt');
          writeFile(newfilepath, data, (newfilepath) => {
            appendFile(newfilepath, containerPath, (newfilepath) => {
              readFile(newfilepath, (data) => {
                data = getLowerCaseSorted(data);
                newfilepath = path.resolve(__dirname, 'sortedData.txt')
                writeFile(newfilepath, data, (newfilepath) => {
                  appendFile(newfilepath, containerPath, (newfilepath) => {
                    let dirPath = path.dirname(newfilepath);
                    deleteAllNewFiles(dirPath, containerPath)
                  })
                })
              })
            })
          })
        })
      })
    })
  })
}
problem2(readFromFilepath, containerPath)