const fs = require('fs');
const path = require('path')
const folderPath = path.resolve(__dirname, 'jsonFiles');
let myData = {
  "2008": 58,
  "2009": 57,
  "2010": 60,
  "2011": 73
}
function makeFolder(folderPath, cb) {
  fs.mkdir(folderPath, function (err) {
    if (err) {
      console.log('failed to create directory', err);
    }else{
      console.log(`new folder ${path.basename(folderPath)} created`);
      cb()
    }
  });
}

function writeFile(folderPath,filename,myData,cb) {
  console.log('started....');
  fs.writeFile(folderPath + `/${filename}`, JSON.stringify(myData), function (err) {
    if (err) {
      console.log('error writing file', err);
    } else {
      console.log('writing file succeeded');
      cb();
    }
  });
}

function deleteFile(filePath){
  console.log('deleting file...')
  fs.unlink(filePath, (err)=>{
    if (err) {
      console.log('error writing file', err);
    } else {
      console.log('deleted file');
    }
  })
}
function deleteFolder(folderPath){
  fs.rmdir(folderPath, function(err) {
    if (err) {
      throw err
    } else {
      console.log("Successfully removed the empty directory!")
    }
  })
}

makeFolder(folderPath, ()=>{
  writeFile(folderPath,'file1.json',myData,()=>{
    writeFile(folderPath,'file2.json',myData,()=>{
      writeFile(folderPath,'file3.json',myData,()=>{
        deleteFile(path.resolve(folderPath, 'file1.json'));
        deleteFile(path.resolve(folderPath, 'file2.json'));
        deleteFile(path.resolve(folderPath, 'file3.json'));
        deleteFolder(folderPath)
      })
    })
  });
});

